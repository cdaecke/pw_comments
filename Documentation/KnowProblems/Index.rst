.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt

.. _knowProblems:

Know problems / Troubleshooter
==============================

fe_user not found
-----------------
If you work with registred users, they will need a special property set in order to be recognized by Extbase. The
following extension explains which field that is and sets it to default: Extension 'fe_users_default_extbase_type'
